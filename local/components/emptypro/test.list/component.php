<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 3600;
}

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if ($arParams["IBLOCK_ID"] < 1) {
    ShowError("IBLOCK_ID IS NOT DEFINED");
    return false;
}

if ($arParams["IBLOCK_ID_2"] < 1) {
    ShowError("IBLOCK_ID_2 IS NOT DEFINED");
    return false;
}

if (!isset($arParams["ITEMS_LIMIT"])) {
    $arParams["ITEMS_LIMIT"] = 3;
}

$arNavParams = array();

if ($arParams["ITEMS_LIMIT"] > 0) {
    $arNavParams = array(
        "nPageSize" => $arParams["ITEMS_LIMIT"],
    );
}

$arNavigation = CDBResult::GetNavParams($arNavParams);

if ($this->StartResultCache(false, array($arNavigation))) {

    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError("IBLOCK_MODULE_NOT_INSTALLED");
        return false;
    }

    $arSort = array(
        "SORT" => "ASC",
        "DATE_ACTIVE_FROM" => "DESC",
        "ID" => "DESC"
    );
    $arFilter = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "ACTIVE_DATE" => "Y"
    );

    $arSelect = array(
        "ID",
        "IBLOCK_ID",
        "NAME",
        "DATE_ACTIVE_FROM",
        "PREVIEW_TEXT",
        "PREVIEW_PICTURE",
        "TIMESTAMP_X",
        "PROPERTY_IB1_USER",
        "PROPERTY_IB1_ELEMENT"
    );

    $rsElement = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);

    if ($arParams["DETAIL_URL"]) {
        $rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
    }

    while ($obElement = $rsElement->GetNextElement()) {

        //get fields
        $arElement = $obElement->GetFields();
        if ($arElement["PREVIEW_PICTURE"]) {
            $arElement["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);
        }

        //get properties
        $arElement["PROPERTIES"] = $obElement->GetProperties();

        //get users
        if (!empty($arElement["PROPERTIES"]['IB1_USER']['VALUE'])) {
            $arElement["USERS"] = array();
            foreach ($arElement["PROPERTIES"]['IB1_USER']['VALUE'] as $userItem) {
                $rsUser = CUser::GetByID($userItem);
                $arUser = $rsUser->Fetch();
                $arElement["USERS"][] = array(
                    "LAST_NAME" => $arUser['LAST_NAME'],
                    "NAME" => $arUser['NAME'],
                    "SECOND_NAME" => $arUser['SECOND_NAME'],
                );
            }
        }

        //get elements
        if (!empty($arElement["PROPERTIES"]['IB1_ELEMENT']['VALUE'])) {
            $arElement["ELEMENTS"] = $arElement["PROPERTIES"]['IB1_ELEMENT']['VALUE'];
        }

        $arResult["ITEMS"][] = $arElement;
    }

    $arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, "Название", $arParams["PAGER_TEMPLATE"], "");

    $this->SetResultCacheKeys(array(
        "ID",
        "IBLOCK_ID",
        "NAV_CACHED_DATA",
        "NAME",
        "IBLOCK_SECTION_ID",
        "IBLOCK",
        "LIST_PAGE_URL",
        "~LIST_PAGE_URL",
        "SECTION",
        "PROPERTIES",
    ));

    $this->IncludeComponentTemplate();

}

?>