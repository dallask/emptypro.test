<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock"))
    return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-" => " "));

$arIBlocks = Array();
$db_iblock = CIBlock::GetList(Array("SORT" => "ASC"), Array("SITE_ID" => $_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "-" ? $arCurrentValues["IBLOCK_TYPE"] : "")));
while ($arRes = $db_iblock->Fetch()) {
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arIBlocks_2 = Array();
$db_iblock_2 = CIBlock::GetList(Array("SORT" => "ASC"), Array("SITE_ID" => $_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE_2"] != "-" ? $arCurrentValues["IBLOCK_TYPE_2"] : "")));
while ($arRes_2 = $db_iblock_2->Fetch()) {
    $arIBlocks_2[$arRes_2["ID"]] = $arRes_2["NAME"];
}

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(

        "IBLOCK_TYPE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPE_1"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "news",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_ID_1"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => '={$_REQUEST["ID"]}',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),
        "IBLOCK_TYPE_2" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPE_2"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "news",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID_2" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_ID_2"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks_2,
            "DEFAULT" => '={$_REQUEST["ID"]}',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),
        "ITEMS_LIMIT" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ITEMS_LIMIT"),
            "TYPE" => "STRING",
            "DEFAULT" => "10",
        ),
        "PAGER_TEMPLATE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("PAGER_TEMPLATE"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),

        "CACHE_TIME" => Array("DEFAULT" => 3600),

    ),
);
?>