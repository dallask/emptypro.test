<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<? if (!empty($arResult['ITEMS'])): ?>
    <div class="row">
        <? foreach ($arResult['ITEMS'] as $k => $arItem): ?>
            <div class="list-item col-lg-12">
                <div class="row">
                    <h3 class="col-xs-12"><?= $arItem['NAME']; ?></h3>
                    <h5 class="col-xs-12">Дата изменения: <?= $arItem['TIMESTAMP_X']; ?></h5>
                    <? if (!empty($arItem['USERS'])): ?>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <ul class="list-group">
                                <li class="list-group-item active">
                                    <strong>Привязанные пользователи:</strong>
                                </li>
                                <? foreach ($arItem['USERS'] as $arUser): ?>
                                    <li class="list-group-item">
                                        <?= (!empty($arUser['LAST_NAME']) ? $arUser['LAST_NAME'] . '&nbsp;' : null); ?>
                                        <?= (!empty($arUser['NAME']) ? $arUser['NAME'] . '&nbsp;' : null); ?>
                                        <?= (!empty($arUser['SECOND_NAME']) ? $arUser['SECOND_NAME'] . '&nbsp;' : null); ?>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arItem['ELEMENTS'])): ?>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <ul class="list-group">
                                <li class="list-group-item active">
                                    <strong>Привязанные элементы:</strong>
                                </li>
                                <? foreach ($arItem['ELEMENTS'] as $elItem): ?>
                                    <li class="list-group-item">
                                        <?= $elItem; ?>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        <? endforeach; ?>
    </div>
    <div class="row text-center">
        <div class="col-lg-12">
            <?= $arResult['NAV_STRING']; ?>
        </div>
    </div>
<? endif; ?>
