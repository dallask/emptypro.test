<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS["IBLOCK_TYPE_1"] = "Тип инфоблока 1";
$MESS["IBLOCK_ID_1"] = "ID инфоблока 1";
$MESS["IBLOCK_TYPE_2"] = "Тип инфоблока 2";
$MESS["IBLOCK_ID_2"] = "ID инфоблока 2";
$MESS["ITEMS_LIMIT"] = "Количество записей на странице";
$MESS["PAGER_TEMPLATE"] = "Шаблон постраничной навигации";

